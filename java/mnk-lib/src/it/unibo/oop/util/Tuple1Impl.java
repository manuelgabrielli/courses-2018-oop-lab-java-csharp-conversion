package it.unibo.oop.util;

class Tuple1Impl<A> extends TupleImpl implements Tuple1<A> {
    public Tuple1Impl(A a) {
        super(a);
    }

    protected Tuple1Impl(Object... items) {
        super(items);
    }

    public A getFirst() {
        return (A) get(0);
    }
}
