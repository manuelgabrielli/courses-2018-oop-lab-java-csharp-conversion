﻿namespace Unibo.Oop.Utils
{
    public interface ITuple<T1> : ITuple
    {
        T1 First { get; }
    }

    public interface ITuple<T1, T2> : ITuple
    {
        T1 First { get; }
        T2 Second { get; }
    }

    public interface ITuple<T1, T2, T3> : ITuple
    {
        T1 First { get; }
        T2 Second { get; }
        T3 Third { get; }
    }

    class TupleImpl<T1> : TupleImpl, ITuple<T1>
    {
        public TupleImpl(T1 value1) 
            : base(new object[] { value1 })
        {
        }

        public T1 First => (T1)this[0];
    }


    class TupleImpl<T1, T2> : TupleImpl, ITuple<T1, T2>
    {
        public TupleImpl(T1 value1, T2 value2) 
            : base(new object[] { value1, value2 })
        {
        }

        public T1 First => (T1)this[0];

        public T2 Second => (T2)this[1];
    }

    class TupleImpl<T1, T2, T3> : TupleImpl, ITuple<T1, T2, T3>
    {
        public TupleImpl(T1 value1, T2 value2, T3 value3) 
            : base(new object[] { value1, value2, value3 })
        {
        }

        public T1 First => (T1)this[0];

        public T2 Second => (T2)this[1];

        public T3 Third => (T3)this[2];
    }
}
