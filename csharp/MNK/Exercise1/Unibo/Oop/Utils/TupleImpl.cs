﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private readonly Object[] items;

        public TupleImpl(object[] args)
        {
            items = args;
        }

        public object this[int i] => items[i];

        public int Length => items.Length;

        public object[] ToArray()
        {
            return items;
        }

        public override string ToString()
        {
            string rsult = "(" + string.Join(", ", items) + ")";
            return "(" + string.Join(",", items) + ")";
        }

        public override bool Equals(object obj)
        {
            return obj != null 
                && obj is TupleImpl 
                && items.SequenceEqual(((TupleImpl)obj).items);
        }

        public override int GetHashCode()
        {
            const int prime = 31;
            int result = 1;
            foreach (Object obj in items)
            {
                result = prime * result + obj.GetHashCode();
            }
            return result;
        }

    }
}
