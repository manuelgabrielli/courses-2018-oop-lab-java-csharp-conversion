using System;

namespace Unibo.Oop.Mnk
{
    public static class MNKMatch
    {
        public static IMNKMatch Of(int m, int n, int k) {
            return new MNKMatchImpl(m, n, k);
        }

        public static IMNKMatch Of(int m, int n, int k, Symbols first) {
            return new MNKMatchImpl(m, n, k, first);
        }
    }
}