using System;
using Unibo.Oop.Utils;

namespace Unibo.Oop.Mnk
{
    public abstract class MNKEventArgs
    {
        private readonly IMNKMatch source;
        private readonly int turn;
        private readonly Symbols player;
        private readonly Tuple<int, int> move;

        public MNKEventArgs(IMNKMatch source, int turn, Symbols player, int i, int j)
        {
            this.source = source;
            this.turn = turn;
            this.player = player;
            this.move = Tuple.Create(i, j);
        }

        public IMNKMatch Source => this.source;

        public int Turn => this.turn;

        public Symbols Player => this.player;

        public Tuple<int, int> Move => this.move;
    }
}