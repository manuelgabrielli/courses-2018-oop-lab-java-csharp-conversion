﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    abstract class AbstractEventSource<TArg> : IEventSource<TArg>, IEventEmitter<TArg>
    {
        protected abstract Collection<EventListener<TArg>> GetEventListeners();

        public IEventSource<TArg> EventSource => this;

        public void Bind(EventListener<TArg> eventListener)
        {
            GetEventListeners().Add(eventListener);
        }

        public void Emit(TArg data)
        { 
            foreach(var eventListener in GetEventListeners())
            {
                eventListener.Invoke(data);
            }
        }

        public void Unbind(EventListener<TArg> eventListener)
        {
            GetEventListeners().Remove(eventListener);
        }

        public void UnbindAll()
        {
            GetEventListeners().Clear();
        }
    }
}
