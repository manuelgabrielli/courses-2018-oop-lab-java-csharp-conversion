﻿using System;
namespace Unibo.Oop.Events
{
    public delegate void EventListener<TArg>(TArg data);
}
