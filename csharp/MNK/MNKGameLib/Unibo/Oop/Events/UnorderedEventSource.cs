﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    class UnorderedEventSource<TArg> : AbstractEventSource<TArg>
    {
        private Collection<EventListener<TArg>> eventListeners = new Collection<EventListener<TArg>>();
        protected override Collection<EventListener<TArg>> GetEventListeners()
        {
            return eventListeners;
        }
    }
}
