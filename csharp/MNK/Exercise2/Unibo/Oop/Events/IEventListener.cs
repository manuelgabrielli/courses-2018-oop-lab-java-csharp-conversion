﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    public interface IEventListener<TArg>
    {
        void OnEvent(TArg data);
    }
}
